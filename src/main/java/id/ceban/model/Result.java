package id.ceban.model;

public class Result<T> {

private int status = -1;
private T model;

public Result(){
}

public void setModel(T model){
        this.model = model;
}

public T getModel(){
        return model;
}

public void setStatus(int status){
        this.status = status;
}

public int getStatus(){
        return status;
}
}
