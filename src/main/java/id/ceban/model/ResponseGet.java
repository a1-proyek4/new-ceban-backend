package id.ceban.model;

public class ResponseGet<T> extends Response {

private T data;
private int size = 0;

public ResponseGet(){
}

public void setData(T data){
        this.data = data;
}

public T getData(){
        return data;
}

public void setSize(int size){
        this.size = size;
}

public int getSize(){
        return size;
}
}
