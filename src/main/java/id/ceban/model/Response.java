package id.ceban.model;

import id.ceban.util.StateMessageUtil;

public class Response {

private String status;
private String message = "";

public Response(){
}

public void setStatus(int status){
        this.status = String.valueOf(status);
        setMessage(StateMessageUtil.getMessage(status));
}

public String getStatus(){
        return status;
}

public void setMessage(String message){
        this.message = message;
}

public void addMessage(String message){
        this.message = this.message + " " + message;
}

public String getMessage(){
        return message;
}
}
