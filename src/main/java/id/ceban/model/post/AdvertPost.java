package id.ceban.model.post;

public class AdvertPost {

private Integer slrNim = 0;
private String adTitle = "";
private String adDesc = "";
private Integer adPrice = 0;
private String adMeetPlace = "";
private Integer ctrId = 0;
private boolean adStatus = true;
private boolean ad_condition = true;

public Integer getSlrNim() {
        return slrNim;
}

public void setSlrNim(Integer slrNim) {
        this.slrNim = slrNim;
}

public String getAdTitle() {
        return adTitle;
}

public void setAdTitle(String adTitle) {
        this.adTitle = adTitle;
}

public String getAdDesc() {
        return adDesc;
}

public void setAdDesc(String adDesc) {
        this.adDesc = adDesc;
}

public Integer getAdPrice() {
        return adPrice;
}

public void setAdPrice(Integer adPrice) {
        this.adPrice = adPrice;
}

public String getAdMeetPlace() {
        return adMeetPlace;
}

public void setAdMeetPlace(String adMeetPlace) {
        this.adMeetPlace = adMeetPlace;
}

public Integer getCtrId() {
        return ctrId;
}

public void setCtrId(Integer ctrId) {
        this.ctrId = ctrId;
}

public boolean getAdStatus(){
        return adStatus;
}

public void setAdStatus(){
        this.adStatus = adStatus;
}

public boolean getAdCondition(){
        return ad_condition;
}

public void setAdCondition(boolean ad_condition){
        this.ad_condition = ad_condition;
}

}
