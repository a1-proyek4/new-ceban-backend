package id.ceban.model.get;

import java.util.List;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.ResultSet;

public class AdvertDetailGet extends AdvertGet {

private int view_value = 0;
private String ad_desc = "";
private String ad_meet_place = "";
private String ad_bump = "";
private String ad_create_date = "";
private boolean ad_stat = false;
private String ad_dead_date = "";
private List < AdvertImageGet > ad_images;
private boolean ad_condition = true;
private String slr_studyprogram = "";
private String slr_contact = "";
private int ctr_id = 0;
private String ctr_name = "";

public AdvertDetailGet() {
}

public AdvertDetailGet(ResultSet rs) throws SQLException {
        setAdId(rs.getInt("ad_id"));
        setAdTitle(rs.getString("ad_title"));
        setAdDesc(rs.getString("ad_desc"));
        setAdPrice(rs.getInt("ad_price"));
        setAdMeetPlace(rs.getString("ad_meet_place"));
        setSlrContact(rs.getString("slr_contact"));
        setAdBump(rs.getString("ad_bump"));
        setAdCreateDate(rs.getString("ad_create_date"));
        setAdStat(rs.getBoolean("ad_stat"));
        setAdDeadDate(rs.getString("ad_end_date"));
        setAdImage(rs.getString("image_img"));
        // setAdCondition(rs.getBoolean("ad_condition"));
        setSlrNim(rs.getInt("slr_nim"));
        setSlrName(rs.getString("slr_name"));
        setSlrStudyProgram(rs.getString("std_name"));
        setSlrDepartment(rs.getString("dpt_name"));
        setCtrId(rs.getInt("ctr_id"));
        setCtrName(rs.getString("ctr_name"));
}

public String getSlrContact() {
        return slr_contact;
}

public String getAdMeetPlace() {
        return ad_meet_place;
}

public String getAdDesc() {
        return ad_desc;
}

public String getAdBump() {
        return ad_bump;
}

public String getAdCreateDate() {
        return ad_create_date;
}


public String getAdDeadDate() {
        return ad_dead_date;
}

public boolean getAdStat() {
        return ad_stat;
}

public List < AdvertImageGet > getAdImages() {
        return ad_images;
}

public int getViewValue() {
        return view_value;
}

public int getCtrId() {
        return ctr_id;
}

public String getCtrName() {
        return ctr_name;
}

public String getSlrStudyProgram() {
        return slr_studyprogram;
}

public void setAdDesc(String ad_desc) {
        this.ad_desc = ad_desc;
}

public void setAdMeetPlace(String ad_meet_place) {
        this.ad_meet_place = ad_meet_place;
}

public void setAdBump(String ad_bump) {
        this.ad_bump = ad_bump;
}

public void setAdCreateDate(String ad_create_date) {
        this.ad_create_date = ad_create_date;
}

public void setAdStat(boolean ad_stat) {
        this.ad_stat = ad_stat;
}

public void setAdDeadDate(String ad_dead_date) {
        this.ad_dead_date = ad_dead_date;
}

public void setAdImages(List < AdvertImageGet > ad_images) {
        this.ad_images = ad_images;
}

public void addImage(AdvertImageGet ad_image) {
        if (ad_images == null) {
                ad_images = new ArrayList();
        }
        ad_images.add(ad_image);
}

public void setViewValue(int view_value) {
        this.view_value = view_value;
}

public void setCtrId(int ctr_id) {
        this.ctr_id = ctr_id;
}

public void setCtrName(String ctr_name) {
        this.ctr_name = ctr_name;
}

public boolean getAdCondition() {
        return ad_condition;
}

public void setAdCondition(boolean ad_condition) {
        this.ad_condition = ad_condition;
}

public void setSlrStudyProgram(String slr_studyprogram) {
        this.slr_studyprogram = slr_studyprogram;
}

public void setSlrContact(String slr_contact) {
        this.slr_contact = slr_contact;
}

}
