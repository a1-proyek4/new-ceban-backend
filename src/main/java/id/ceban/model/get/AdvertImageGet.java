package id.ceban.model.get;

public class AdvertImageGet {

private String image = "";

public AdvertImageGet(String image){
        this.image = image;
}

public String getImage(){
        return image;
}

public void setImage(String image){
        this.image = image;
}
}
