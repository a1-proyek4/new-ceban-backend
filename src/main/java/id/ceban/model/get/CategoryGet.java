package id.ceban.model.get;

import id.ceban.model.post.CategoryPost;
import java.util.ArrayList;
import java.util.List;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CategoryGet extends CategoryPost {

private List<CategoryGet> ctr_sub_categories;

public CategoryGet(){
}

public CategoryGet(ResultSet rs) throws SQLException {
        if(rs != null) {
                setCtrId(rs.getInt("ctr_id"));
                setCtrName(rs.getString("ctr_name"));
                setCtrImage(rs.getString("ctr_image"));
                setCtrDesc(rs.getString("ctr_dsc"));
                setCtrAdCount(rs.getInt("ctr_ad_count"));
        }
}

public List<CategoryGet> getCtrSubCategories(){
        return ctr_sub_categories;
}

public void setCtrSubCategories(List<CategoryGet> ctr_sub_categories){
        this.ctr_sub_categories = ctr_sub_categories;
}

public void addSubCategories(CategoryGet sub_category){
        if(ctr_sub_categories == null) {
                ctr_sub_categories = new ArrayList();
        }
        ctr_sub_categories.add(sub_category);
}



}
