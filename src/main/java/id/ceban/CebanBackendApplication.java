package id.ceban;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.boot.builder.SpringApplicationBuilder;

import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.context.annotation.Bean;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@SpringBootApplication
public class CebanBackendApplication extends SpringBootServletInitializer {

public static void main(String[] args) throws Exception {
        SpringApplication.run(CebanBackendApplication.class, args);
}

@Override
protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(CebanBackendApplication.class);
}

@Bean
public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurerAdapter() {
                       @Override
                       public void addCorsMappings(CorsRegistry registry) {
                               registry.addMapping("/").allowedOrigins("http://localhost:4200");
                               registry.addMapping("/").allowedOrigins("http://localhost:8090");
                       }
        };
}

}
