package id.ceban.service;

import id.ceban.dao.BaseDao;
import id.ceban.model.get.CategoryGet;
import id.ceban.model.post.CategoryPost;
import id.ceban.model.Result;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import id.ceban.cons.StateCons;
import id.ceban.util.StringUpdateBuilderUtil;

public class CategoryService extends BaseService
        implements BaseDao<CategoryPost, CategoryGet, CategoryGet> {

public CategoryService(){

}

@Override
public Result<Integer> save(
        CategoryPost model){

        Result<Integer> result = new Result();
        if(getDatabase().getResult() == StateCons.SUCCESS) {
                try{
                        String sql = "INSERT INTO category (cat_ctr_id, ctr_name, ctr_image, ctr_dsc, ctr_ad_count) VALUES ("+ model.getCtrId() +", '"+ model.getCtrName() +"', '"+ model.getCtrImage() +"', '"+ model.getCtrDesc() +"', "+ model.getCtrAdCount() +") RETURNING ctr_id;";
                        setResultSet(getDatabase().executeQuery(sql));
                        if(getResultSet().next()) {
                                result.setModel(new Integer(getResultSet().getInt("ctr_id")));
                                result.setStatus(StateCons.SUCCESS);
                        }else{
                                result.setStatus(StateCons.FAILED);
                        }
                        getResultSet().close();
                }catch(SQLException e) {
                        result.setStatus(StateCons.NO_RECORD);
                }
        }else{
                result.setStatus(StateCons.FAILED);
        }
        return result;
}

@Override
public Result<Boolean> delete(
        int modelId){

        Result<Boolean> result = new Result();
        if(getDatabase().getResult() == StateCons.SUCCESS) {
                try{
                        String sql = "DELETE FROM category WHERE ctr_id = "+ modelId +";";
                        boolean isUpdateSuccess = (getDatabase().executeUpdate(sql) > 0);
                        result.setModel(new Boolean(isUpdateSuccess));
                        result.setStatus(isUpdateSuccess ? StateCons.SUCCESS : StateCons.FAILED);
                }catch(SQLException e) {
                        result.setStatus(StateCons.NO_RECORD);
                }
        }else{
                result.setStatus(StateCons.FAILED);
        }
        return result;
}

@Override
public Result<Boolean> update(
        CategoryPost model,
        int modelId){

        StringUpdateBuilderUtil sUpdateBuilderUtil = new StringUpdateBuilderUtil();

        if(model.getCtrId() != 0){
                sUpdateBuilderUtil.addNewString("cat_ctr_id = "+ model.getCtrId());
        }
        if(!model.getCtrName().equals("")){
                sUpdateBuilderUtil.addNewString("ctr_name = '"+ model.getCtrName() +"'");
        }
        if(!model.getCtrDesc().equals("")){
                sUpdateBuilderUtil.addNewString("ctr_dsc = '"+ model.getCtrDesc() +"'");
        }
        if(!model.getCtrImage().equals("")){
                sUpdateBuilderUtil.addNewString("ctr_image = '"+ model.getCtrImage() +"'");
        }
        if(model.getCtrAdCount() != 0){
                sUpdateBuilderUtil.addNewString("ctr_ad_count = "+ model.getCtrAdCount());
        }
        
        Result<Boolean> result = new Result();
        if(getDatabase().getResult() == StateCons.SUCCESS) {
                try{
                        String sql = "UPDATE category SET "+ sUpdateBuilderUtil.getResultString() +" WHERE ctr_id = "+ modelId +";";
                        boolean isDeleteSuccess = (getDatabase().executeUpdate(sql) > 0);
                        result.setModel(new Boolean(isDeleteSuccess));
                        result.setStatus(isDeleteSuccess ? StateCons.SUCCESS : StateCons.FAILED);
                }catch(SQLException e) {
                        result.setStatus(StateCons.NO_RECORD);
                }
        }else{
                result.setStatus(StateCons.FAILED);
        }
        return result;
}

@Override
public Result<CategoryGet> get(
        int modelId){
        Result<CategoryGet> result = new Result();
        if(getDatabase().getResult() == StateCons.SUCCESS) {
                try{
                        String sql = "SELECT ctr_id, cat_ctr_id, ctr_name, ctr_image, ctr_dsc, ctr_ad_count FROM category WHERE ctr_id = '"+ modelId +"' LIMIT 1;";
                        setResultSet(getDatabase().executeQuery(sql));
                        if(getResultSet().next()) {
                                result.setModel(new CategoryGet(getResultSet()));
                                result.setStatus(StateCons.SUCCESS);
                        }else{
                                result.setStatus(StateCons.NO_RECORD);
                        }
                        getResultSet().close();
                }catch(SQLException e) {
                        result.setStatus(StateCons.NO_RECORD);
                }
        }else{
                result.setStatus(StateCons.FAILED);
        }
        return result;
}

@Override
public Result<List<CategoryGet> > getList(
        String search_query,
        String field,
        String sort,
        int page,
        int limit){
        return getList(0, search_query, field, sort, page, limit);
}

private Result<List<CategoryGet> >  getList(
        int ctrParentId,
        String search_query,
        String field,
        String sort,
        int page,
        int limit){

        Result<List<CategoryGet> > result = new Result();
        int i = (limit * (page-1));
        int n = (limit * page);
        if(field.equals("")) field = "category.ctr_name";
        if(sort.equals("")) sort = "ASC";
        if(page == 0 ) page = 1;
        if(getDatabase().getResult() == StateCons.SUCCESS) {
                try{
                        List<CategoryGet> list = new ArrayList();
                        String sql = "SELECT ctr_id, cat_ctr_id, ctr_name, ctr_image, ctr_dsc, ctr_ad_count FROM category "+ (ctrParentId < 0 ? "" : (" WHERE cat_ctr_id " + (ctrParentId < 1 ? "IS NULL" : ("= " + ctrParentId)))) + " ORDER BY "+ field +" "+ sort +", category.ctr_id ASC "+ (limit > 0 ? "LIMIT "+ limit : "") + " OFFSET "+i+";";
                        setResultSet(getDatabase().executeQuery(sql));
                        while(getResultSet().next()) {
                                list.add(new CategoryGet(getResultSet()));
                        }
                        result.setModel(list);
                        if(list.size() > 0) {
                                result.setStatus(StateCons.SUCCESS);
                        }else{
                                result.setStatus(StateCons.NO_RECORD);
                        }
                        getResultSet().close();
                }catch(SQLException e) {
                        result.setStatus(StateCons.NO_RECORD);
                }
        }
        else{
                result.setStatus(StateCons.FAILED);
        }
        return result;
}

public Result<List<CategoryGet> > getList(
        int ctrParentId){
        return getList(ctrParentId, "", "", "", 0, 0);
}

public Result<List<CategoryGet> > getList(){
        return getList(-1, "", "", "", 0, 0);
}

public Result<Integer> count(int modelId){
        Result<Integer> result = new Result();
        result.setModel(new Integer(0));
        if(getDatabase().getResult() == StateCons.SUCCESS) {
                try{
                        String sql = "SELECT COUNT(ctr_id) AS jumlah FROM category "+ ( modelId == 0 ? "" : "WHERE cat_ctr_id = '"+ modelId +"'")+";";
                        setResultSet(getDatabase().executeQuery(sql));
                        if(getResultSet().next()) {
                                result.setModel(new Integer(getResultSet().getInt("jumlah")));
                                result.setStatus(StateCons.SUCCESS);
                        }else{
                                result.setStatus(StateCons.NO_RECORD);
                        }
                        getResultSet().close();
                }catch(SQLException e) {
                        result.setStatus(StateCons.NO_RECORD);
                }
        }else{
                result.setStatus(StateCons.FAILED);
        }
        return result;
}

}

