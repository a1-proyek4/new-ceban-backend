package id.ceban.service;

import id.ceban.util.DBConnectorUtil;
import java.sql.SQLException;
import java.sql.ResultSet;

public class BaseService {

private DBConnectorUtil db;
private ResultSet rs;

public void connectToDatabase(){
        db = new DBConnectorUtil();
}

public DBConnectorUtil getDatabase(){
        return db;
}

public void setDatabase(DBConnectorUtil db){
        this.db = db;
}

public void closeConnection() throws SQLException {
        if(db != null) {
                db.closeStatement();
                db.closeConnection();
        }
}

public void setResultSet(ResultSet rs){
        this.rs = rs;
}

public ResultSet getResultSet(){
        return this.rs;
}

}
