package id.ceban.service;

import id.ceban.dao.BaseDao;
import id.ceban.model.get.AdvertGet;
import id.ceban.model.get.AdvertDetailGet;
import id.ceban.model.get.AdvertImageGet;
import id.ceban.model.post.AdvertPost;
import id.ceban.model.Result;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import id.ceban.cons.StateCons;

public class AdvertService extends BaseService
        implements BaseDao < AdvertPost, AdvertGet, AdvertDetailGet > {

public AdvertService() {

}

@Override
public Result < Integer > save(AdvertPost model) {
        return null;
}

@Override
public Result < Boolean > delete(int modelId) {
        return null;
}

@Override
public Result < Boolean > update(AdvertPost model, int modelId) {
        return null;
}

@Override
public Result < AdvertDetailGet > get(int modelId) {
        Result < AdvertDetailGet > result = new Result();
        if (getDatabase().getResult() == StateCons.SUCCESS) {
                try {
                        String sql = "SELECT advert.ad_id, advert.ad_title, advert.ad_desc, advert.ad_price, (SELECT image.image_img FROM image WHERE image.ad_id = advert.ad_id LIMIT 1), advert.ad_meet_place, advert.ad_bump, advert.ad_create_date, advert.ad_stat, advert.ad_end_date, advert.slr_nim, advert.ad_condition, seller.slr_name, seller.slr_contact, studyprogram.std_name, category.ctr_id, category.ctr_name, department.dpt_name FROM advert INNER JOIN seller on seller.slr_nim = advert.slr_nim INNER JOIN advert_category ON advert_category.ad_id = advert_category.ad_id INNER JOIN category ON advert_category.ctr_id = category.ctr_id INNER JOIN studyprogram ON seller.std_id = studyprogram.std_id INNER JOIN department ON studyprogram.dpt_id = department.dpt_id WHERE advert.ad_id = '"+ modelId +"' LIMIT 1;";
                        ResultSet rs = getDatabase().executeQuery(sql);
                        if (rs.next()) {
                                result.setModel(new AdvertDetailGet(rs));
                                result.setStatus(StateCons.SUCCESS);
                        } else {
                                result.setStatus(StateCons.NO_RECORD);
                        }
                        rs.close();
                } catch (SQLException e) {
                        result.setStatus(StateCons.SQL_EXCEPTION);
                }
        } else {
                result.setStatus(StateCons.FAILED);
        }
        return result;
}

public Result < List < AdvertGet > > getList(int ctr_id, String search_query, String field, String sort, int page, int limit) {
        Result < List < AdvertGet > > result = new Result();
        int i = (limit * (page - 1));
        int n = (limit * page);
        search_query = "(LOWER(ad_title) LIKE '%" + search_query + "%' OR LOWER(ad_desc) LIKE '%" + search_query + "%')";
        if (field.equals("")) field = "advert.ad_id";
        if (sort.equals("")) sort = "ASC";
        if (page == 0) page = 1;
        if (limit == 0) limit = 10;
        boolean verified = false;
        if (getDatabase().getResult() == StateCons.SUCCESS) {
                try {
                        List < AdvertGet > list = new ArrayList();
                        String sql = "SELECT advert.ad_id, advert.ad_title, advert.ad_price, (SELECT image.image_img FROM image WHERE image.ad_id = advert.ad_id LIMIT 1), advert.slr_nim, seller.slr_name, department.dpt_name FROM advert INNER JOIN advert_category ON advert_category.ad_id = advert.ad_id INNER JOIN category ON category.ctr_id = advert_category.ctr_id INNER JOIN seller ON advert.slr_nim = seller.slr_nim INNER JOIN studyprogram ON seller.std_id = studyprogram.std_id INNER JOIN department ON studyprogram.dpt_id = department.dpt_id WHERE(" + search_query + ")" + (ctr_id > 0 ? "AND advert_category.ctr_id = '"+ ctr_id +"'" : "" )  + " ORDER BY "+ field +" "+ sort +", advert.ad_bump DESC LIMIT "+limit+" OFFSET "+i+";";
                        ResultSet rs = getDatabase().executeQuery(sql);
                        while (rs.next()) {
                                list.add(new AdvertGet(rs));
                        }
                        result.setModel(list);
                        if (list.size() > 0) {
                                result.setStatus(StateCons.SUCCESS);
                        } else {
                                result.setStatus(StateCons.NO_RECORD);
                        }
                        rs.close();
                } catch (SQLException e) {
                        result.setStatus(StateCons.SQL_EXCEPTION);
                }
        } else {
                result.setStatus(StateCons.FAILED);
        }
        return result;
}

@Override
public Result < List < AdvertGet > > getList(String search_query, String field, String sort, int page, int limit) {
        return getList(0, search_query, field, sort, page, limit);
}

public Result<Integer> count(int modelId){
        Result<Integer> result = new Result();
        result.setModel(new Integer(0));
        if(getDatabase().getResult() == StateCons.SUCCESS) {
                try{
                        String sql = "SELECT COUNT(advert.ad_id) AS jumlah FROM advert INNER JOIN advert_category ON advert_category.ad_id = advert.ad_id "+ ( modelId == 0 ? "" : "WHERE advert_category.ctr_id = '"+ modelId +"'")+";";
                        setResultSet(getDatabase().executeQuery(sql));
                        if(getResultSet().next()) {
                                result.setModel(new Integer(getResultSet().getInt("jumlah")));
                                result.setStatus(StateCons.SUCCESS);
                        }else{
                                result.setStatus(StateCons.NO_RECORD);
                        }
                        getResultSet().close();
                }catch(SQLException e) {
                        result.setStatus(StateCons.NO_RECORD);
                }
        }else{
                result.setStatus(StateCons.FAILED);
        }
        return result;
}

}
