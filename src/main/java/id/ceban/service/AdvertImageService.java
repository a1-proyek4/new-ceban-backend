package id.ceban.service;

import id.ceban.dao.BaseDao;
import id.ceban.model.get.AdvertImageGet;
import id.ceban.model.Result;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import id.ceban.cons.StateCons;

public class AdvertImageService extends BaseService
        implements BaseDao<AdvertImageGet, AdvertImageGet, AdvertImageGet> {

public AdvertImageService(){

}

@Override
public Result<Integer> save(AdvertImageGet model){
        return null;
}

@Override
public Result<Boolean> delete(int modelId){
        return null;
}

@Override
public Result<Boolean> update(AdvertImageGet model, int modelId){
        return null;
}

@Override
public Result<AdvertImageGet> get(int modelId){
        return null;
}

@Override
public Result<List<AdvertImageGet> > getList(String search_query, String field, String sort, int page, int limit){
        return getList(0, search_query, field, sort, page, limit);
}

private Result<List<AdvertImageGet> >  getList(int ad_id, String search_query, String field, String sort, int page, int limit){
        Result<List<AdvertImageGet> > result = new Result();
        int i = (limit * (page-1));
        int n = (limit * page);
        search_query = "(image_img LIKE '%" + search_query + ")";
        if(field.equals("")) field = "image.image_id";
        if(sort.equals("")) sort = "DESC";
        if(page == 0 ) page = 1;
        if(limit == 0) limit = 5;
        if(getDatabase().getResult() == StateCons.SUCCESS) {
                try{
                        List<AdvertImageGet> list = new ArrayList();
                        String sql = "SELECT image_img FROM image " + (ad_id > 0 ?  "WHERE ad_id = '" + ad_id + "'" : "" ) + " AND " + search_query +  " ORDER BY "+ field +" "+ sort +" LIMIT "+limit+" OFFSET "+i+";";
                        ResultSet rs = getDatabase().executeQuery(sql);
                        while(rs.next()) {
                                list.add(new AdvertImageGet(rs.getString("image_img")));
                        }
                        result.setModel(list);
                        if(list.size() > 0) {
                                result.setStatus(StateCons.SUCCESS);
                        }else{
                                result.setStatus(StateCons.NO_RECORD);
                        }
                        rs.close();
                }catch(SQLException e) {
                        result.setStatus(StateCons.NO_RECORD);
                }
        }
        else{
                result.setStatus(StateCons.FAILED);
        }
        return result;
}

public Result<List<AdvertImageGet> > getList(int ctrParentId){
        return getList(ctrParentId, "", "", "", 0, 0);
}

public Result<Integer> count(int modelId){
         Result<Integer> result = new Result();
        result.setModel(new Integer(0));
        if(getDatabase().getResult() == StateCons.SUCCESS) {
                try{
                        String sql = "SELECT COUNT(image_id) AS jumlah FROM image "+ ( modelId == 0 ? "" : " WHERE ad_id = '"+ modelId +"'")+";";
                        setResultSet(getDatabase().executeQuery(sql));
                        if(getResultSet().next()) {
                                result.setModel(new Integer(getResultSet().getInt("jumlah")));
                                result.setStatus(StateCons.SUCCESS);
                        }else{
                                result.setStatus(StateCons.NO_RECORD);
                        }
                        getResultSet().close();
                }catch(SQLException e) {
                        result.setStatus(StateCons.NO_RECORD);
                }
        }else{
                result.setStatus(StateCons.FAILED);
        }
        return result;
}

}
