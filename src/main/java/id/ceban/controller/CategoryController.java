package id.ceban.controller;

import java.util.List;
import java.util.ArrayList;
import java.sql.SQLException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.http.MediaType;
import org.springframework.web.multipart.MultipartFile;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;
import id.ceban.model.get.AdvertGet;
import id.ceban.model.get.CategoryGet;
import id.ceban.model.post.CategoryPost;
import id.ceban.model.ResponseGet;
import id.ceban.model.Response;
import id.ceban.model.ResponsePost;
import id.ceban.model.Result;
import id.ceban.cons.StateCons;
import id.ceban.util.ImageUploaderUtil;
import id.ceban.service.AdvertService;
import id.ceban.service.CategoryService;
import java.sql.SQLException;
import java.io.IOException;
import java.io.FileNotFoundException;

@RestController
public class CategoryController {

CategoryService service = new CategoryService();

@ApiOperation(value = "getCategoryById", nickname = "get Category by Id")
@RequestMapping(method = RequestMethod.GET, path = "categories/{ctr_id}", produces = "application/json")
@ApiResponses(value = {
                @ApiResponse(code = 200, message = "Success", response = CategoryGet.class),
                @ApiResponse(code = 401, message = "Unauthorized"),
                @ApiResponse(code = 403, message = "Forbidden"),
                @ApiResponse(code = 404, message = "Not Found"),
                @ApiResponse(code = 500, message = "Failure")
        })
public ResponseGet<CategoryGet> getCategoryById(
        @PathVariable int ctr_id,
        @RequestParam(name="w", defaultValue="false", required = false) boolean with_sub_categories){
        ResponseGet<CategoryGet> response = new ResponseGet();
        try{
                service.connectToDatabase();
                Result<CategoryGet> result = service.get(ctr_id);
                response.setStatus(result.getStatus());

                if(with_sub_categories) {
                        Result<List<CategoryGet> > result2 = service.getList(result.getModel().getCtrId());
                        result.getModel().setCtrSubCategories(result2.getModel());
                }
                response.setData(result.getModel());
                service.closeConnection();
        }catch(SQLException e) {
                response.setStatus(StateCons.SQL_EXCEPTION);
        }
        return response;
}

@ApiOperation(value = "getCategories", nickname = "get Categories")
@RequestMapping(method = RequestMethod.GET, path = "categories", produces = "application/json")
@ApiResponses(value = {
                @ApiResponse(code = 200, message = "Success", response = CategoryGet.class),
                @ApiResponse(code = 401, message = "Unauthorized"),
                @ApiResponse(code = 403, message = "Forbidden"),
                @ApiResponse(code = 404, message = "Not Found"),
                @ApiResponse(code = 500, message = "Failure")
        })
public ResponseGet<List<CategoryGet> > getCategories(
        @RequestParam(name="w", defaultValue="false", required = false) boolean with_sub_categories,
        @RequestParam(name="q", defaultValue="", required = false) String query,
        @RequestParam(name="f", defaultValue="category.ctr_name", required = false) String field,
        @RequestParam(name="s", defaultValue="ASC", required = false) String sort,
        @RequestParam(name="p", defaultValue="1", required = false) int page,
        @RequestParam(name="l", defaultValue="10", required = false) int limit){
        ResponseGet<List<CategoryGet> > response = new ResponseGet();
        int i = (limit * (page-1));
        int n = (limit * page);
        try{
                service.connectToDatabase();
                Result<List<CategoryGet> > result = service.getList(query, field, sort, page, limit);
                response.setStatus(result.getStatus());
                if(with_sub_categories) {
                        for(int j = 0; j < result.getModel().size(); j++) {
                                Result<List<CategoryGet> > result2 = service.getList(result.getModel().get(j).getCtrId());
                                result.getModel().get(j).setCtrSubCategories(result2.getModel());
                        }
                }
                response.setData(result.getModel());
                service.closeConnection();
        }catch(SQLException e) {
                response.setStatus(StateCons.SQL_EXCEPTION);
        }
        return response;
}

@ApiOperation( value = "postNewCategory", nickname = "post New Category")
@RequestMapping( method = RequestMethod.POST, path = "categories/new",
                 consumes = "multipart/form-data", produces = "application/json")
@ApiResponses(value = {
                @ApiResponse(code = 200, message = "Success", response = Integer.class),
                @ApiResponse(code = 401, message = "Unauthorized"),
                @ApiResponse(code = 403, message = "Forbidden"),
                @ApiResponse(code = 404, message = "Not Found"),
                @ApiResponse(code = 500, message = "Failure")
        })
public ResponsePost postNewCategory(
        @RequestPart(name = "catCtrId", required = false) String catCtrId,
        @RequestPart(name = "ctrName", required = true) String ctrName,
        @RequestPart(name = "ctrDesc", required = true) String ctrDesc,
        @RequestPart(name = "ctrImage", required = false) MultipartFile ctrImage){

        ResponsePost response = new ResponsePost();
        CategoryPost model = new CategoryPost();
        model.setCtrId(Integer.parseInt(catCtrId));
        model.setCtrName(ctrName);
        model.setCtrDesc(ctrDesc);
        try{
                service.connectToDatabase();
                ImageUploaderUtil uploader = null;
                if(ctrImage != null) {
                        uploader = new ImageUploaderUtil();
                        model.setCtrImage(uploader.upload(ctrImage));
                }
                Result<Integer> result = service.save(model);
                response.setStatus(result.getStatus());
                response.setId(result.getModel());
                service.closeConnection();
        }catch(SQLException e) {
                response.setStatus(StateCons.SQL_EXCEPTION);
        }catch(FileNotFoundException e) {
                response.setStatus(StateCons.FILE_NOT_FOUND);
        }catch(IOException e) {
                response.setStatus(StateCons.IO_EXCEPTION);
        }
        return response;
}

@ApiOperation( value = "postEditCategory", nickname = "post Edit Category")
@RequestMapping( method = RequestMethod.POST, path = "categories/edit",
                 consumes = "multipart/form-data", produces = "application/json")
@ApiResponses(value = {
                @ApiResponse(code = 200, message = "Success"),
                @ApiResponse(code = 401, message = "Unauthorized"),
                @ApiResponse(code = 403, message = "Forbidden"),
                @ApiResponse(code = 404, message = "Not Found"),
                @ApiResponse(code = 500, message = "Failure")
        })
public Response postEditCategory(
        @RequestPart(name = "ctrId", required = true) String ctrId,
        @RequestPart(name = "catCtrId", required = false) String catCtrId,
        @RequestPart(name = "ctrName", required = false) String ctrName,
        @RequestPart(name = "ctrDesc", required = false) String ctrDesc,
        @RequestPart(name = "ctrImage", required = false) MultipartFile ctrImage){

        Response response = new Response();
        CategoryPost model = new CategoryPost();
        int i = 0;
        if(ctrName != null) {
                model.setCtrName(ctrName);
                i++;
        }
        if(ctrDesc != null) {
                model.setCtrDesc(ctrDesc);
                i++;
        }
        if(i > 0) {
                try{
                        service.connectToDatabase();
                        ImageUploaderUtil uploader = null;
                        if(ctrImage != null) {
                                uploader = new ImageUploaderUtil();
                                model.setCtrImage(uploader.upload(ctrImage));
                        }
                        Result<Boolean> result = service.update(model, Integer.parseInt(ctrId));
                        response.setStatus(result.getStatus());
                        service.closeConnection();
                }catch(SQLException e) {
                        response.setStatus(StateCons.SQL_EXCEPTION);
                }catch(FileNotFoundException e) {
                        response.setStatus(StateCons.FILE_NOT_FOUND);
                }catch(IOException e) {
                        response.setStatus(StateCons.IO_EXCEPTION);
                }
        }else{
                response.setStatus(StateCons.INPUT_NOT_VALID);
        }
        return response;
}

@ApiOperation( value = "postUpdateAdCountCategory", nickname = "post Update Ad Count Category")
@RequestMapping( method = RequestMethod.POST, path = "categories/update_ad_count", produces = "application/json")
@ApiResponses(value = {
                @ApiResponse(code = 200, message = "Success"),
                @ApiResponse(code = 401, message = "Unauthorized"),
                @ApiResponse(code = 403, message = "Forbidden"),
                @ApiResponse(code = 404, message = "Not Found"),
                @ApiResponse(code = 500, message = "Failure")
        })
public Response postUpdateAdCountCategory(){

        Response response = new Response();
        Result<Boolean> result = new Result();
        try{
                service.connectToDatabase();
                Result<List<CategoryGet>> categories = service.getList();
                int i = 0;
                AdvertService advertService = new AdvertService();
                advertService.setDatabase(service.getDatabase());
                while (i < categories.getModel().size() && 
                (result.getStatus() == StateCons.PRE_STATE || result.getStatus() == StateCons.SUCCESS)) {
                        int jml = advertService.count(categories.getModel().get(i).getCtrId()).getModel();    
                        CategoryPost categoryPost = new CategoryPost();
                        categoryPost.setCtrAdCount(jml);
                        result = service.update(categoryPost, categories.getModel().get(i).getCtrId());
                        response.setStatus(result.getStatus()); 
                        i++;
                }  
                service.closeConnection();
        }catch(SQLException e) {
                response.setStatus(StateCons.SQL_EXCEPTION);
        }
        return response;
}

}
