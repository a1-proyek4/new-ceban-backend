package id.ceban.controller;

import java.util.List;
import java.util.ArrayList;
import java.sql.SQLException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.http.MediaType;
import org.springframework.web.multipart.MultipartFile;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;
import id.ceban.model.get.AdvertGet;
import id.ceban.model.get.AdvertDetailGet;
import id.ceban.model.get.AdvertImageGet;
import id.ceban.model.post.AdvertPost;
import id.ceban.model.ResponseGet;
import id.ceban.model.Result;
import id.ceban.cons.StateCons;
import id.ceban.service.AdvertService;
import id.ceban.service.AdvertImageService;

@RestController
public class AdvertController {

AdvertService service = new AdvertService();

@ApiOperation(value = "getAds", nickname = "get All Ads")
@RequestMapping(method = RequestMethod.GET, path = "ads", produces = "application/json")
@ApiResponses(value = {
                @ApiResponse(code = 200, message = "Success", response = AdvertGet.class),
                @ApiResponse(code = 401, message = "Unauthorized"),
                @ApiResponse(code = 403, message = "Forbidden"),
                @ApiResponse(code = 404, message = "Not Found"),
                @ApiResponse(code = 500, message = "Failure")
        })
public ResponseGet<List<AdvertGet> > getAllAds(
        @RequestParam(name="q", defaultValue="", required = false) String query,
        @RequestParam(name="f", defaultValue="advert.ad_id", required = false) String field,
        @RequestParam(name="s", defaultValue="ASC", required = false) String sort,
        @RequestParam(name="p", defaultValue="1", required = false) int page,
        @RequestParam(name="l", defaultValue="10", required = false) int limit,
        @RequestParam(name="t", defaultValue="new", required = false) String type,
        @RequestParam(name="v", defaultValue="true", required = false) boolean verified){
        ResponseGet<List<AdvertGet> > response = new ResponseGet();
        int i = (limit * (page-1));
        int n = (limit * page);
        try{
                service.connectToDatabase();
                Result<List<AdvertGet> > result = service.getList(query.toLowerCase(), field, sort, page, limit);
                response.setStatus(result.getStatus());
                response.setData(result.getModel());
                service.closeConnection();
        }catch(SQLException e) {
                response.setStatus(StateCons.SQL_EXCEPTION);
        }
        return response;
}

@ApiOperation(value = "getAdById", nickname = "get Ad Detail")
@RequestMapping(method = RequestMethod.GET, path = "ads/{ad_id}", produces = "application/json")
@ApiResponses(value = {
                @ApiResponse(code = 200, message = "Success", response = AdvertDetailGet.class),
                @ApiResponse(code = 401, message = "Unauthorized"),
                @ApiResponse(code = 403, message = "Forbidden"),
                @ApiResponse(code = 404, message = "Not Found"),
                @ApiResponse(code = 500, message = "Failure")
        })
public ResponseGet<AdvertDetailGet> getAdsById(@PathVariable int ad_id){
        ResponseGet<AdvertDetailGet> response = new ResponseGet();
        try{
                service.connectToDatabase();
                Result<AdvertDetailGet> result = service.get(ad_id);
                if(result.getModel() != null) {
                        AdvertImageService imageService = new AdvertImageService();
                        Result<List<AdvertImageGet> > resultImages = imageService.getList(ad_id);
                        result.getModel().setAdImages(resultImages.getModel());
                        imageService.closeConnection();
                }
                response.setStatus(result.getStatus());
                response.setData(result.getModel());
                service.closeConnection();
        }catch(SQLException e) {
                response.setStatus(StateCons.SQL_EXCEPTION);
        }
        return response;
}

@ApiOperation(value = "getAdsByCategory", nickname = "get Ads by Category")
@RequestMapping(method = RequestMethod.GET, path = "categories/{ctr_id}/ads", produces = "application/json")
@ApiResponses(value = {
                @ApiResponse(code = 200, message = "Success", response = AdvertGet.class),
                @ApiResponse(code = 401, message = "Unauthorized"),
                @ApiResponse(code = 403, message = "Forbidden"),
                @ApiResponse(code = 404, message = "Not Found"),
                @ApiResponse(code = 500, message = "Failure")
        })
public ResponseGet<List<AdvertGet> > getAdsbyCategory(
        @PathVariable int ctr_id,
        @RequestParam(name="q", defaultValue="", required = false) String query,
        @RequestParam(name="f", defaultValue="advert.ad_id", required = false) String field,
        @RequestParam(name="s", defaultValue="DESC", required = false) String sort,
        @RequestParam(name="p", defaultValue="1", required = false) int page,
        @RequestParam(name="l", defaultValue="10", required = false) int limit,
        @RequestParam(name="v", defaultValue="false", required = false) boolean verified){
        ResponseGet<List<AdvertGet> > response = new ResponseGet();
        int i = (limit * (page-1));
        int n = (limit * page);
        try{
                service.connectToDatabase();
                Result<List<AdvertGet> > result = service.getList(ctr_id, query, field, sort, page, limit);
                response.setStatus(result.getStatus());
                response.setData(result.getModel());
                service.closeConnection();
        }catch(SQLException e) {
                response.setStatus(StateCons.SQL_EXCEPTION);
        }
        return response;
}

}
