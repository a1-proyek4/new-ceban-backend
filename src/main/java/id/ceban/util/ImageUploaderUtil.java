package id.ceban.util;

import id.ceban.cons.AuthCons;
import org.springframework.web.multipart.MultipartFile;
import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.File;
import java.util.Map;

public class ImageUploaderUtil {

private Cloudinary cloud;

public ImageUploaderUtil(){
        connect(AuthCons.CLOUDINARY_CLOUD_NAME,
                AuthCons.CLOUDINARY_API_KEY,
                AuthCons.CLOUDINARY_API_SECRET);
}

public ImageUploaderUtil(String cloudName, String apiKey, String apiSecret){
        connect(cloudName, apiKey, apiSecret);
}

public void connect(String cloudName, String apiKey, String apiSecret){
        Map config = ObjectUtils.asMap(
                "cloud_name", cloudName,
                "api_key", apiKey,
                "api_secret", apiSecret);
        cloud = new Cloudinary(config);
}

public String upload(MultipartFile file) throws IOException {
        if(cloud == null) {
                return "";
        }else{
                File convertedFile = convert(file);
                if(convertedFile != null) {
                        Map uploadResult = cloud.uploader().upload(convertedFile, ObjectUtils.emptyMap());
                        return String.valueOf(uploadResult.get("url"));
                }else{
                        return "";
                }
        }
}

public File convert(MultipartFile file) throws IOException, FileNotFoundException {
        File convFile = new File(file.getOriginalFilename());
        convFile.createNewFile();
        FileOutputStream fos = new FileOutputStream(convFile);
        fos.write(file.getBytes());
        fos.close();
        return convFile;
}

}
