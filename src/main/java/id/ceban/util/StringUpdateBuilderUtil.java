package id.ceban.util;

import java.util.ArrayList;
import java.util.List;

public class StringUpdateBuilderUtil {

List<String> strings;

public StringUpdateBuilderUtil(){
    
}

public void addNewString(String string){
    if(strings == null){
        strings = new ArrayList<>();
    }
    strings.add(string);
}

public String getResultString(){
    String result = "";
    for(int i = 0; i < strings.size(); i++){
        if(i > 0){
            result += ", ";
        }
        result += strings.get(i);
    }
    return result;
}

}
