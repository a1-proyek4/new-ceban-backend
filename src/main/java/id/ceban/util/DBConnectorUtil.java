package id.ceban.util;

import java.sql.Connection;
import java.sql.Statement;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.lang.Exception;
import id.ceban.cons.AuthCons;
import id.ceban.cons.StateCons;

public class DBConnectorUtil {

private Connection connection;
private Statement statement;

int result = StateCons.PRE_STATE;

public Statement getStatement(){
        return statement;
}

public Connection getConnection(){
        return connection;
}

public DBConnectorUtil(){
        String url = AuthCons.POSTGRES_URL;
        String username = AuthCons.POSTGRES_USERNAME;
        String password =  AuthCons.POSTGRES_PASSWORD;
        connect(url, username, password);
}

public DBConnectorUtil(String url, String username, String password){
        connect(url, username, password);
}

public void connect(String url, String username, String password){
        try {
                Class.forName("org.postgresql.Driver");
                try{
                        connection = DriverManager.getConnection(url, username, password);
                        statement = connection.createStatement();
                        result = StateCons.SUCCESS;
                }catch (SQLException e) {
                        result = StateCons.SQL_EXCEPTION;
                }catch (Exception e) {
                        result = StateCons.FAILED;
                }
        } catch (java.lang.ClassNotFoundException e) {
                result = StateCons.LIBRARY_NOT_FOUND;
        }
}

public int getResult(){
        return result;
}

public boolean execute(String sql) throws SQLException {
        System.out.println("exe : " + sql);
        return statement.execute(sql);
}

public ResultSet executeQuery(String sql) throws SQLException {
        System.out.println("exeQ : " + sql);
        return statement.executeQuery(sql);
}

public int executeUpdate(String sql) throws SQLException {
        System.out.println("exeUp : " + sql);
        return statement.executeUpdate(sql);
}

public void closeStatement() throws SQLException {
        statement.close();
}

public void closeConnection() throws SQLException {
        connection.close();
}
}
