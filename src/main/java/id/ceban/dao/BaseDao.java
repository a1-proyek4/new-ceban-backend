package id.ceban.dao;

import java.util.List;
import id.ceban.model.Result;

public interface BaseDao<P, G, GD> {

public Result<Integer> save(P model);

public Result<Boolean> delete(int modelId);

public Result<Boolean> update(P model, int modelId);

public Result<GD> get(int modelId);

public Result<List<G> > getList(String search_query, String field, String sort, int page, int limit);

public Result<Integer> count(int modelId);

}
