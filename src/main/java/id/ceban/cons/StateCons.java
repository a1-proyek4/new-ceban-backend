package id.ceban.cons;

public class StateCons {

public static int PRE_STATE = -1;
public static int SUCCESS = 0;
public static int FAILED = 1;
public static int SQL_EXCEPTION = 2;
public static int INPUT_NOT_VALID = 3;
public static int DATABASE_ERROR = 4;
public static int IO_EXCEPTION = 5;
public static int LIBRARY_NOT_FOUND = 6;
public static int FILE_NOT_FOUND = 7;
public static int NO_RECORD = 8;
public static int NOT_DEFINED = 100;

}
